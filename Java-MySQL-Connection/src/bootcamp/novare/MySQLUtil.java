package bootcamp.novare;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


public final class MySQLUtil {

    private static final String CREATE_DB;

    static {
        CREATE_DB = "CREATE DATABASE IF NOT EXISTS %s;";
    }

    public static boolean createDatabaseIfNotExist(String schema, Connection connection) {
        try {
            Statement stmt = connection.createStatement();
            stmt.execute( String.format(CREATE_DB, schema) );
            stmt.close();

            return true;
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());

            return false;
        }
    }

    /**
     * Create a connection to the database server without a selected
     * database.
     *
     * @param host
     * @param username
     * @param password
     * @return database connection
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static Connection createConnection (String host, String username, String password) throws Exception {
        Class.forName("com.mysql.jdbc.Driver");
        return DriverManager.getConnection(host, username, password);
    }

    /**
     * Overloaded method to create a connection on a specific database
     * e.g.
     *      jdbc:mysql://localhost:3306/my_sample_db;
     *
     * @param dbName
     * @param host
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    public static Connection createConnection (String dbName, String host, String username, String password) throws Exception {
        return createConnection( host +  dbName, username, password );
    }
}
