package bootcamp.novare;

import java.sql.*;

public class Main {


    private static String HOST = "jdbc:mysql://localhost:3306/";
    private static String USER = "root";
    private static String PASS = "root";

    private Connection databaseConnection;

    public static void main(String[] args) {
        try {
            Main main = new Main( "bootcamp_db" );
            main.createSampleTable();

            main.createData("e-0001", "Pedro");
            main.createData( "e-0002", "juan" );

            main.read();
            main.update( "e-0001", "butcha" );
            main.read();

            main.delete( "e-0001" );
            main.read();
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public Main( String database ) {
        try {
            Connection connection = MySQLUtil.createConnection( HOST, USER, PASS );
            boolean databaseCreated = MySQLUtil.createDatabaseIfNotExist( database, connection );

            if (databaseCreated) {
                System.out.println("Database created: " + databaseCreated);

                //connect to the newly created database
                databaseConnection = MySQLUtil.createConnection( database, HOST, USER, PASS) ;
            }

            connection.close();
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public void createSampleTable() throws SQLException {
        final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS employees (employeeId VARCHAR(20) NOT NULL PRIMARY KEY," +
                                                            "employeeName VARCHAR(255) NOT NULL)";
        Statement stmt = databaseConnection.createStatement();
        stmt.execute( CREATE_TABLE );

        stmt.close();
    }

    // CRUD OPERATIONS
    public void createData( String empId, String empName ) throws SQLException {
        PreparedStatement pstmt = databaseConnection.prepareStatement( "INSERT INTO employees VALUES(?, ?)" );
        pstmt.setString(1, empId);
        pstmt.setString(2, empName);
        pstmt.execute();

        pstmt.close();

        System.out.println("successfully inserted");
    }

    public void read() throws SQLException {
        Statement stmt = databaseConnection.createStatement();
        ResultSet rs = stmt.executeQuery( "SELECT * FROM employees" );

        System.out.println("Employees:");
        while (rs.next()) {
            System.out.printf("%s - %s%n", rs.getString( "employeeId" ), rs.getString( "employeeName") );
        }

        System.out.println();

        stmt.close();
        rs.close();
    }

    public void update(String empIdToUpdate, String newName) throws SQLException {
        PreparedStatement pstmt = databaseConnection.prepareStatement( "UPDATE employees SET employeeName = ? WHERE employeeId = ?" );
        pstmt.setString(1, newName);
        pstmt.setString(2, empIdToUpdate);
        pstmt.execute();

        pstmt.close();

        System.out.println("successfully updated");
    }

    public void delete(String empId) throws SQLException {
        PreparedStatement pstmt = databaseConnection.prepareStatement( "DELETE FROM employees WHERE employeeId = ?" );
        pstmt.setString(1, empId);
        pstmt.execute();

        pstmt.close();

        System.out.println("successfully deleted");
    }
}
